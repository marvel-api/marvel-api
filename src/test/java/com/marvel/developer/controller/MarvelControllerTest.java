package com.marvel.developer.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.marvel.developer.utils.Utils;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("testing")
class MarvelControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void characters() throws Exception {
		var root = Utils.ROOT;
		ResultActions ra = mockMvc.perform(get(root + "characters").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		assertNotNull(ra);
	}

	@Test
	void character() throws Exception {
		var root = Utils.ROOT;
		ResultActions ra = mockMvc.perform(get(root + "characters/1").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		assertNotNull(ra);
	}

}