package com.marvel.developer.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marvel.developer.model.Registry;
import com.marvel.developer.repository.MarvelRepository;

@Service
public class MarvelService implements IMarvelService {

	@Autowired
	private MarvelRepository repository;

	@Override
	public List<Registry> findAll() {
		String hora = new SimpleDateFormat("HH:mm").format(new Date());
		repository.save(new Registry("/v1/public/characters", hora));
		return repository.findAll();
	}

	@Override
	public Registry findById(Long id) {
		Registry character;
		String hora = new SimpleDateFormat("HH:mm").format(new Date());
		character = repository.save(new Registry("/v1/public/characters/{characterId}", hora));
		return character;
	}
}
