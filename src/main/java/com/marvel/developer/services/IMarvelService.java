package com.marvel.developer.services;

import java.util.List;

import com.marvel.developer.model.Registry;

public interface IMarvelService {

	List<Registry> findAll();

	Registry findById(Long id);
}
