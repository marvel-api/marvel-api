package com.marvel.developer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.marvel.developer.model.Registry;
import com.marvel.developer.repository.MarvelRepository;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EntityScan("com.marvel.developer.model")
@EnableJpaRepositories("com.marvel.developer.repository")
@Slf4j
public class MarvelApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarvelApplication.class, args);
	}

	@Bean
	CommandLineRunner start(MarvelRepository marvelRepository) {
		return args -> {
			String hora = new SimpleDateFormat("HH:mm").format(new Date());
			Registry characters = new Registry(1L, "/v1/public/characters", hora);
			marvelRepository.save(characters);
			marvelRepository.findAll().forEach(c -> log.info("Character: {}", c));
		};
	}

}
