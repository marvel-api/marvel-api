package com.marvel.developer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marvel.developer.model.Registry;
import com.marvel.developer.services.IMarvelService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping(value = "/v1/public/characters", produces = MediaType.APPLICATION_JSON_VALUE)
public class MarvelController {

	@Autowired
	private IMarvelService service;

	@GetMapping
	public ResponseEntity<?> findAll() {
		List<Registry> characters = service.findAll();
		if (characters != null && !characters.isEmpty()) {
			return new ResponseEntity<>(characters, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/{characterId}")
	public ResponseEntity<?> findAll(@PathVariable String characterId) {
		Long id;
		try {
			id = Long.parseLong(characterId);
		} catch (NumberFormatException e) {
			return new ResponseEntity<>("El ID debe ser numerico", HttpStatus.NOT_FOUND);
		}
		Registry character = service.findById(id);
		if (character != null) {
			return new ResponseEntity<>(character, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
