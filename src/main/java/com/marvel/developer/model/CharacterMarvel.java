package com.marvel.developer.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CharacterMarvel implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
}
