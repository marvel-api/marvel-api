package com.marvel.developer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marvel.developer.model.Registry;

public interface MarvelRepository extends JpaRepository<Registry, Long> {

	Registry save(Character person);

	List<Registry> findAll();

}
